﻿using System;

namespace Noroff_Task_4
{
    class Program
    {
        //Simple, but efficient program that prints two nested rectangles
        static void Main(string[] args)
        {
            Run();
        }

        /*Prompts the user for height and length of rectangle
        and calls PrintRectangle()*/
        static void Run()
        {
            int inputLength;
            int inputHeight;

            Console.Write("Enter the length of the rectangle, minimum 5: ");
            inputLength = Convert.ToInt32(Console.ReadLine()); //No validation tho D: 

            Console.Write("Enter the height of the rectangle, minimum 5: ");
            inputHeight = Convert.ToInt32(Console.ReadLine()); //No validation tho D: 

            PrintRectangle(inputLength, inputHeight);
        }
        static void PrintRectangle(int length, int height)
        {
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < height; j++)
                {
                    if (i == 0 || j == 0 || i == length - 1 || j == height - 1) {
                        Console.Write("#");
                    } else if ((i == 2 || i == length - 3) && (j != 1 && j != height - 2)) {
                        Console.Write("#");
                    } else if ((j == 2 || j == height - 3) && (i != 1 && i != length - 2)) {
                        Console.Write("#");
                    } else {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();

            }
        }
    }
}
